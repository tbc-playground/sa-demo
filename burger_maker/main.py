from typing import Any
import uuid
import random

from fastapi import FastAPI, HTTPException

burger_recipes = {
    "cheeseburger": [
        "bun",
        "red onion",
        "tomato",
        "mustard",
        "salad",
        "cheddar",
        "steak",
    ],
    "bacon": ["bun", "bacon", "garlic sauce", "brie", "steak", "tomato"],
    "farmer": ["bun", "red onion", "bbq sauce", "stilton", "chicken", "tomato"],
    "fish": ["bun", "tartare sauce", "cucumber", "old gouda", "fish fillet"],
    "veggie": [
        "bun",
        "goat cheese",
        "tomato confit",
        "red onion",
        "super-secret veggie steak",
    ],
    "frenchie": ["baguette", "butter", "brie", "smoked ham"],
}

app = FastAPI()


@app.get("/health")
def health_check() -> dict[str, Any]:
    return {"status": "ok"}


@app.get("/recipes")
def gime_the_menu() -> list[str]:
    return list(burger_recipes.keys())


@app.get("/recipes/{recipe}")
def gime_the_ingredients(recipe: str) -> list[str]:
    ingredients = burger_recipes.get(recipe)
    if ingredients is None:
        raise HTTPException(status_code=404, detail=f"No such recipe: {recipe}")
    return ingredients


@app.delete("/burgers/{recipe}")
def prepare_a_burger(recipe: str) -> dict[str, Any]:
    if recipe == "any":
        # pick one at random
        recipe = random.choice(list(burger_recipes.keys()))

    ingredients = burger_recipes.get(recipe)
    if ingredients is None:
        raise HTTPException(status_code=404, detail=f"No such recipe: {recipe}")

    return {
        "name": recipe,
        "id": str(uuid.uuid1()),
        "ingredients": list(
            map(lambda name: {"id": str(uuid.uuid1()), "name": name}, ingredients)
        ),
    }
