FROM registry.hub.docker.com/library/python:3.9 as requirements-stage
 
WORKDIR /tmp

# hadolint ignore=DL3013
RUN pip install --no-cache-dir poetry

COPY ./pyproject.toml ./poetry.lock* /tmp/

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

###########

FROM registry.hub.docker.com/library/python:3.9-slim-buster

# use default port 5000 for GitLab Auto DevOps
# see: https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-build-using-a-dockerfile
ENV PORT=5000
WORKDIR /code
 
COPY --from=requirements-stage /tmp/requirements.txt /code/requirements.txt

RUN apt-get -y update && apt-get -y upgrade \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./burger_maker /code/burger_maker

EXPOSE ${PORT}
# hadolint ignore=DL3025
CMD uvicorn burger_maker.main:app --host=0.0.0.0 --port=${PORT}
