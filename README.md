# Burger API project

This project implements a Burger API.

It is implemented in Python (3.9), uses the [FastAPI](https://fastapi.tiangolo.com/) library and deploys to Kubernetes.

The project implements a state-of-the-art DevOps pipeline, made of various pieces:

* [GitLab Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) templates for build and many test tools,
* [to-be-continuous](https://to-be-continuous.gitlab.io/doc/) templates for `pylint`, `pytest`, SonarQube, Kubernetes deployment and [Postam](https://www.postman.com/) API testing.
