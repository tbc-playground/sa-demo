import json

from fastapi.testclient import TestClient

from burger_maker import main

client = TestClient(main.app)


def test_menu_should_be_as_expected():
    response = client.get("/recipes")
    expected = json.dumps(
        ["cheeseburger", "bacon", "farmer", "fish", "veggie", "frenchie"],
        separators=(",", ":"),
    )

    assert response.status_code == 200
    assert response.text == expected


def test_cheeseburger_should_be_as_expected():
    response = client.delete("/burgers/cheeseburger")

    assert response.status_code == 200
    body = response.json()
    assert body["name"] == "cheeseburger"
    assert len(body["ingredients"]) == 7
    assert body["ingredients"][0]["name"] == "bun"
    assert body["ingredients"][1]["name"] == "red onion"
    assert body["ingredients"][2]["name"] == "tomato"
    assert body["ingredients"][3]["name"] == "mustard"
    assert body["ingredients"][4]["name"] == "salad"
    assert body["ingredients"][5]["name"] == "cheddar"
    assert body["ingredients"][6]["name"] == "steak"


def test_anyburger_should_be_delivered():
    response = client.delete("/burgers/any")
    assert response.status_code == 200


def test_nonexistingburger_should_fail():
    response = client.delete("/burgers/nosuchrecipe")
    assert response.status_code == 404
